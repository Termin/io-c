import { Tank } from '../src/UObjects/Tank';
import { CheckFuelCommand } from '../src/Commands/CheckFuelCommand';
import { BurnebleFuelAdapter, FUEL_KEY, CONSUMPTION_FUEL_KEY } from '../src/Adapters/BurnebleFuelAdapter';

describe('Тестируем CheckFuelCommand', () => {
  const isBurneble = new BurnebleFuelAdapter (new Tank({ [FUEL_KEY]: 100, [CONSUMPTION_FUEL_KEY]: -3 }))
  const isNotBurneble = new BurnebleFuelAdapter (new Tank({ [FUEL_KEY]: 2, [CONSUMPTION_FUEL_KEY]: -3 }))
  const commandIsBurneble = new CheckFuelCommand(isBurneble)
  const commandIsNotBurneble = new CheckFuelCommand(isNotBurneble)
  it('Проверка на наличае нужного колличества топлива', () => {
    expect(() => commandIsBurneble.execute()).not.toThrow()
  })
  it('Проверка на CommandExeption если нет нужного колличества топлива', () => {
    expect(() => commandIsNotBurneble.execute()).toThrow()
  })
});
