import { Tank } from '../src/UObjects/Tank';
import { BurnFuelCommand } from '../src/Commands/BurnFuelCommand';
import { FUEL_KEY, CONSUMPTION_FUEL_KEY, BurnebleFuelAdapter } from '../src/Adapters/BurnebleFuelAdapter';
describe('Тестируем BurnFuelCommand', () => {
  const isBurneble = new BurnebleFuelAdapter (new Tank({ [FUEL_KEY]: 100, [CONSUMPTION_FUEL_KEY]: -3 }))
  const isNotBurneble = new BurnebleFuelAdapter (new Tank({ [FUEL_KEY]: 2, [CONSUMPTION_FUEL_KEY]: -3 }))
  const noFuel = new BurnebleFuelAdapter (new Tank({ [CONSUMPTION_FUEL_KEY]: -3 }))
  const noConsamption = new BurnebleFuelAdapter (new Tank({ [FUEL_KEY]: 2}))

  const commandIsBurneble = new BurnFuelCommand(isBurneble)
  const commandIsNotBurneble = new BurnFuelCommand(isNotBurneble)
  const commandIsNotFuel = new BurnFuelCommand(noFuel)
  const commandIsNotConsamption = new BurnFuelCommand(noConsamption)

  it('Проверка на выполение комманды BurnFuelCommand', () => {
    const VALUE = isBurneble.getFuel() + isBurneble.getConsumptionFuel() 
    commandIsBurneble.execute()
    expect(isBurneble.getFuel()).toBe(VALUE)
  })

  it('BurnFuelCommand должна выполняться и с отрицательными значениями', () => {
    const VALUE = isNotBurneble.getFuel() + isNotBurneble.getConsumptionFuel() 
    commandIsNotBurneble.execute()
    expect(isNotBurneble.getFuel()).toBe(VALUE)
  })

  it(`Exseption - если нет ключа ${FUEL_KEY}`, () => {
    expect(() => commandIsNotFuel.execute()).toThrow()
  })
  
  it(`Exseption - если нет ключа ${CONSUMPTION_FUEL_KEY}`, () => {
    expect(() => commandIsNotConsamption.execute()).toThrow()
  })
});
