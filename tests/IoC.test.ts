import { IoC } from '../src/IoC/IoC';
import { scopes } from '../src/IoC/scopes';
import { ICommand } from '../src/interfaces/ICommand';

describe('Проверка работы  IoC контейнера', () => {
  const REGISTER_KEY = 'IoC.Register';
  const registerCallback = IoC.map[REGISTER_KEY];
  it('Регестрация нового элемента в IoC.map', () => {
    expect(registerCallback).not.toBeUndefined();
  });
  describe('Провера регистрации и вызова нового элемента', () => {
    const KEY_NEW_ELEM = 'test';
    it('Зарегестрировать новый элемент в IoC.map', () => {
      IoC.resolve<ICommand>(REGISTER_KEY, KEY_NEW_ELEM, (...args: any[]) => {
        return args.reverse();
      }).execute();
      expect(IoC.map[KEY_NEW_ELEM]).not.toBeUndefined();
    });
    it('Новый зарегестрированный элемент в IoC.map отдает нужную функцию', () => {
      const value = IoC.resolve(KEY_NEW_ELEM, 1, 2, 3, 4, 5)
      expect(value).toEqual([5, 4, 3, 2, 1]);
    });
  });

  describe('Работа с IoC скоупами', () => {
    const ID = '25';
    const KEY_NEW_ELEM = 'test';
    it('Создание нового скоупа', () => {
      IoC.resolve<ICommand>('Scopes.New', ID, {test2: 'test'}).execute();
      expect(scopes[ID].test2).toBe('test');
    });
    it('Переключить на другой скоуп', () => {
      IoC.resolve<ICommand>('Scopes.Current', ID).execute();
      IoC.resolve<ICommand>(REGISTER_KEY, KEY_NEW_ELEM, (...args: any[]) => {
        return args;
      }).execute();
      const value = IoC.resolve(KEY_NEW_ELEM, 1, 2, 3, 4, 5, 6, 7)
      expect(value).toEqual([1, 2, 3, 4, 5, 6, 7]);
    });
    it('Работает если переключиться 1 скоуп', () => {
      IoC.resolve<ICommand>('Scopes.Current', '1').execute();
      const value = IoC.resolve('test', 1, 2, 3)
      expect(value).toEqual([3,2,1])
    })
  });
});
