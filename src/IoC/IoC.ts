import { scopes, DEFAULT_ID } from './scopes';

export class IoC {
  static map: any = scopes[DEFAULT_ID];
  static resolve<T>(...args: any[]): T {
    const key = args.shift();
    const callBack = IoC.map[key];
    return callBack(...args);
  }
}
