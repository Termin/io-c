import { ICommand } from '../interfaces/ICommand';
import { RotatebleAdapter } from '../Adapters/RotatebleAdepter';

const FULL_ANGLE = 360

export class RotateCommand implements ICommand {
  private adapter: RotatebleAdapter;
  constructor(adapter: RotatebleAdapter) {
    this.adapter = adapter
  }
  execute() {
    rotate(this.adapter)
  } 
}

function rotate(adapter: RotatebleAdapter): void {
  const result = (adapter.getRotateAngle() + adapter.getRotateVelosity()) % FULL_ANGLE
  adapter.setRotateAngle(result)
}