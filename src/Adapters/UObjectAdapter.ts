import { IUObject } from '../interfaces/IUObject';
export abstract class UObjectAdapter {
  protected UObject: IUObject
  constructor (UObject: IUObject) {
    this.UObject = UObject
  }
}