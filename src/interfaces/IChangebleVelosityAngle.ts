import { Vector } from './IMovable';

export interface IChangebleVelosityAngle {
  setVelocity(newVelosity: Vector): void;
  getVelosity(): Vector;
  getFactorOfSpeed(): number;
  getRotateAngle(): number;
  setRotateAngle(newValue: number): void;
  getRotateVelosity(): number;
}
