export interface IUObject {
  getProperty: (key: string) => any;
  setProperty: (key: string, value: any) => void;
}
