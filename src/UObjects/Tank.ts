import { IUObject } from '../interfaces/IUObject';
export class Tank implements IUObject {
  private _attributes: any
  constructor (attributes: any) {
    this._attributes = attributes
  }
  getProperty(key: string) {
    return this._attributes[key]
  }
  setProperty(key: string, value: any) {
    this._attributes[key] = value
  }

  get attr () {
    return this._attributes
  }
}